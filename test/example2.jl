using Shapley, MLJ, Shapley.ComputationalResources, DataFrames
import RDatasets

using Shapley: MonteCarlo

using MLJDecisionTreeInterface: RandomForestClassifier


iris = RDatasets.dataset("datasets", "iris")

y, X = unpack(iris, ==(:Species), ∈([:SepalLength, :SepalWidth, :PetalLength, :PetalWidth]))

m = machine(RandomForestClassifier(), X, y)

fit!(m)

ϕ = shapley(X -> predict(m, X), MonteCarlo(CPUThreads(), 1024), X)

#df = DataFrame(Shapley.summary(X -> predict(m, X), MonteCarlo(CPUThreads(), 1024), X))

