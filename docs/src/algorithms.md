# [Algorithms and Parallelism](@id algorithms)
As has been discussed, in nearly all cases Shapley values are impossible to compute
exactly, both due to computational intractibility and lack of knowledge of the true
distribution.

This package's API allows users to use a common interface.  It consists of `Algorithm`
objects which store parameters associated with the particular algorithm.

Additionally, these objects describe what parallelism, if any, should be used in the
computation.  These are associated with the algorithms because different algorithms may
have different ways of utilizing parallelism.

This package uses
[ComputationalResources.jl](https://github.com/timholy/ComputationalResources.jl) to
describe which computational resources, and which degree of parallelism should be used in
a computation.

For the time being, the package only provides the `Shaplye.MonteCarlo` algorithm.

## Available Algorithms

### `Shapley.MonteCarlo`
```@docs
Shapley.MonteCarlo
```

**Computational Resources:**
- `CPU1()`: Single threaded.
- `CPUThreads()`: Independent iterations of the monte carlo algorithm will be computed on
    separate threads.  See the [Julia
    multi-threading](https://docs.julialang.org/en/v1/manual/multi-threading/)
    documentation for instructions on how to set the number of CPU threads available to
    the Julia process.

### `Shapley.KernelSHAP`
```@docs
Shapley.KernelSHAP
```

**Computational Resources:**
- `CPU1()`: Single threaded.
- `CPUThreads()`: Linear regressions will be performed on different threads.

!!! note

    Using `KernelSHAP` effictively will require a good understanding of the "lasso"
    (``L_1,L_2`` regularized linear regressions) regression from the Lasso.jl package.  In
    particular see [the documentation for
    `fit`](https://juliastats.org/Lasso.jl/stable/lasso/#StatsBase.fit). The fit method
    called by `KernelSHAP` is `fit(LassoModel, X, y; kwargs...)`.
