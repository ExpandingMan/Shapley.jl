"""
    Shapley

This module contains methods for computing Shapley values for machine learning methods.

Only the functino `shapley` is exported.
"""
module Shapley

using Statistics, LinearAlgebra, Random, Tables, ComputationalResources, Distributions, ThreadsX
# this gets used in some type assertions
using Tables: Columns

# TODO not 100% the RNG in parallel is completely safe

const FeatureIndex = Union{Integer,Symbol}


"""
    Algorithm{P<:AbstractResource}

Abstract type from which algorithms for computing Shapley values are descended. The type parameter is the type of
computational resource to be used, but individual algorithms are not required to support all possible resources.
"""
abstract type Algorithm{P<:AbstractResource} end

"""
    isindependent(a::Algorithm)

Whether the algorithm can compute Shapley values independently.  If the result is true, then it is efficient to call the
algorithm for only a particular feature.  If false, when calling `shapley` on a particular feature, the Shapley values
for other features will be computed and discarded.  It is therefore not recommended to call `shapley` for subsets of
features for algorithms for which `isindependent` is false.
"""
isindependent(a::Algorithm) = false

"""
    supports_classification(a::Algorithm)

Whether the algorithm supports classifiers.  If false, the user is responsible for providing a prediction function which
returns numerical arrays.
"""
supports_classification(a::Algorithm) = false

"""
    featureindex(X, j::Union{Integer,Symbol})

Return the index corresponding to key `j` in the columns of `Tables`-compatible `X`.  E.g. if `j` is an integer this
simply returns `j`.
"""
featureindex(X, j::Integer) = j
featureindex(X, j::Symbol) = Tables.columnindex(X, j)

"""
    compatibletable(X)

Create a `Tables.Columns` object from `X`.  This function is provided so that tables and arrays can be handled the same way.
"""
compatibletable(X) = Tables.istable(X) ? Tables.Columns(X) : throw(ArgumentError("expected $X to be a table"))
compatibletable(X::AbstractArray) = Tables.Columns(Tables.table(X))

"""
    ntcopy(x)

Copy an object.  This defaults to `Base.copy` except for on `NamedTuple`s where it copies each column.
"""
ntcopy(x) = copy(x)
ntcopy(nt::NamedTuple) = (;(k=>copy(v) for (k,v) ∈ pairs(nt))...)


"""
    shapley(predict, a, X)
    shapley(predict, a, X, j)
    shapley(predict, a, X, Z)
    shapley(predict, a, X, j, Z)

Compute the Shapley values of the points in a dataset `Z`, using the dataset `X` as an empirical estimate of the distribution
of the points in `Z`.  If `Z` is not provided, Shapley values will be computed for the points in `X`.  There are a variety
of inequivalent methods for computing Shapley values, and these should be specified with `a`, a `Shapley.Algorithm` object
which specifies how the computation should be done.  The `predict` function will be used to make predictions on the
dataset.  The model object being evaluated is encoded via the `predict` function.

If the prediction returned by `predict` is deterministic, and therefore an array of numbers, the returned Shapley values
will be numerical.  If the prediction is probabilistic, it is expected that `predict` returns an array of
`Distributions.Sampleable` objects and `Shapley` will return a table.  The columns of this table will be named according
to the domain of the distributions returned, as determined via `Distributions.support`.

All algorithms will assume that the function `predict` is most efficient when operating on batches of data.  That is,
they will attempt to minimize the number of separate calls to `predict`.

See the [Shapley.jl documentation](https://expandingman.gitlab.io/Shapley.jl/) for more details and examples.

## Arguments
- `predict`: A function which takes a Tables compatible object (or a matrix) and returns a vector of determinstic predictions
    as real numbers or of probabilistic predictions as `Distributions.Sampleable` objects.
- `a`: A `Shapley.Algorithm` object which specifies how the computation should be performed. See `subtypes(Shapley.Algorithm)`
    for a list of available algorithms.
- `X`: An input dataset which is used by the Shapley algorithms for the purpose of providing an empirical estimation of the
    distribution of independent variables.  Typically this would be a test or training set.
- `j`: The feature to calculate the Shapley values for, as a column of `X`. Should be an integer or `Symbol`.  If not
    provided, Shapley values will be computed for all columns.
- `Z`: The set of data points to compute the Shapley values of.  If not provided, `X` will be used.

## Examples
```julia
m = machine(RandomForestRegressor(), X, y)  # an MLJ machine object
ϕ = shapley(ξ -> predict(m, ξ), Shapley.MonteCarlo(512), X)
```
"""
function shapley(predict::Function, a::Algorithm, X::AbstractArray, Z=X)
    reduce(hcat, (shapley(predict, a, X, j, Z) for j ∈ 1:size(X,2)))
end
function shapley(predict::Function, a::Algorithm, X, Z=X)
    js = Tables.schema(X).names
    (;(j=>shapley(predict, a, X, j, Z) for j ∈ js)...)
end

_boxtable(cols::AbstractVector, n::Integer) = (;(Symbol(c)=>Vector{Float64}(undef, n) for c ∈ cols)...)
_boxtable(cols::AbstractArray{<:Integer}, n::Integer) = Matrix{Float64}(undef, n, length(cols))
_boxassign(tab, σ, v) = (Tables.Columns(tab)[Symbol(string(σ))] .= v)  # to cover CategoricalVariable args for σ
_boxassign(tab, σ::Integer, v) = (Tables.Columns(tab)[σ] .= v)
_boxassign(tab::AbstractMatrix, σ, v) = (tab[:, σ] .= v)

"""
    a ⊟ b

A subtraction operation which can optionally take as arguments arrays of `Distributions.Sampleable` objects.  In this case,
the returned result will be a table the columns of which are determined by the domain (`Distributions.support`) of the
returned distribtions.  This is needed by Shapley algorithms to handle the objects returned by `predict` functions in both
deterministic and probabilistic cases.

For all other cases, this operation is equivalent to `a - b`.
"""
function (⊟)(a::AbstractArray{<:Sampleable}, b::AbstractArray{<:Sampleable})
    s = support(first(a))
    tab = _boxtable(s, length(a))
    for σ ∈ s
        _boxassign(tab, σ, [pdf(a[i], σ) - pdf(b[i], σ) for i ∈ 1:length(a)])
    end
    tab
end
(⊟)(a, b) = a - b

"""
    similartable(tab)

Creates a named tuple the values of which are obtained by calling `similar` on the corresponding columns of the table `tab`.
"""
function similartable(tab)
    cs = Tables.Columns(tab)
    (;(n=>similar(cs[n]) for n ∈ keys(cs))...)
end

# this is a hack to get tableop to work with scalars in second arg... not totally convinced it's a good idea
_getindextrick(a, idx) = Tables.getcolumn(a, idx)
_getindextrick(a::Number, idx) = a

"""
    tableop(op, args...)

Apply `op` element-wise on arguments `args`.  The first argument must be a table that complies with the Tables interface.
If a subsequent element is a scalar, this calls `op.(args[1], args[2:end]...)`.
"""
tableop(op::Function, a::AbstractArray, args...) = op.(a, args...)
function tableop(op::Function, args...)
    tab = similartable(first(args))
    for idx ∈ eachindex(Tables.Columns(tab))
        tab[idx] .= op.((_getindextrick(a, idx) for a ∈ args)...)
    end
    tab
end

"""
    ncolumns(X)

Get the number of columns of either a matrix or a table.
"""
ncolumns(X::AbstractMatrix) = size(X,2)
ncolumns(X) = length(propertynames(X))

"""
    nrows(X)

Get the number of rows of either a matrix or a table.
"""
nrows(X::AbstractMatrix) = size(X,1)
nrows(X) = Tables.rowcount(X)

"""
    colnames(X)

Get the column names of either a matrix or a table.  If a matrix, uses default Tables.jl column names.
"""
colnames(X::AbstractArray) = Tables.schema(Tables.table(X)).names
colnames(X::AbstractArray{<:NamedTuple}) = keys(first(X))
colnames(X) = Tables.schema(X).names

"""
    _map(res::AbstractResource)

Get the appropriate `map` function associated with the resource.  For `CPU1` this is `Base.map` whereas for `CPUThreads`
it is `ThreadsX.map`.
"""
_map(::AbstractResource) = map
_map(::CPUThreads) = ThreadsX.map



include("montecarlo.jl")
include("kernelshap.jl")


"""
    opnamedtuple(op, ϕ, n=Symbol(string(op)))

Create a named tuple the elements of which are the operation `op` applied to the columns of Tables compatible object `ϕ`.
The names of the columns of the resulting table will be those of the columns of `ϕ` prepended by `n*'_'`.
"""
opnamedtuple(op::Function, ϕ::AbstractArray, n::Symbol=Symbol(string(op))) = (;n=>op(ϕ),)
function opnamedtuple(op::Function, ϕ, nn::Symbol=Symbol(string(op)))
    cs = Tables.Columns(ϕ)
    (;(Symbol(string(nn)*"_"*string(n))=>op(cs[n]) for n ∈ keys(cs))...)
end

# TODO we really need an easier way to get summaries from disaggregated data...
# the current methods pretty much guarantee you'll always have to re-compute everything

"""
    summary(ϕ, js=colnames(ϕ); statistics=(;))
    summary(predict, a, X; statistics=(;), data=X)

Create a summary table describing the statistics of Shapley values for features `js` from the prediction function
`predict` using `X` as an empirical estimate of the distribution of the data.  By default, this will provide summaries
of the mean of the absolute value, the root mean square, and the standard deviation.  Users can specify additional statistics
via the named tuple `statistics`.

Note that the table returned is a `Vector` of `NamedTuple`.  As a Tables compatible object, this can easily by converted
to a more convenient form, for example `DataFrame(summary(args...))` for a `DataFrame`.

Currently `summary` does not work for classifiers because of the different output type.
In those cases one should start from `shapley`.

## Arguments
- `ϕ`: A table of Shapley values, e.g. as returned from the `shapley` function.
- `predict`: A function which takes a Tables compatible object (or a matrix) and returns a vector of determinstic predictions
    as real numbers or of probabilistic predictions as `Distributions.Sampleable` objects.
- `a`: A `Shapley.Algorithm` object which specifies how the computation should be performed. See `subtypes(Shapley.Algorithm)`
    for a list of available algorithms.
- `X`: An input dataset which is used by the Shapley algorithms for the purpose of providing an empirical estimation of the
    distribution of independent variables.  Typically this would be a test or training set.
- `js`: The feature (or features, as an iterable) to compute Shapley statistics for.
- `data`: The dataset to compute Shapley statistics for.
- `statistics`: A named tuple of additional statistics.  The keys should be the names of the columns in the summary table
    and the values should be functions which accept a `Vector{<:Real}`.
"""
function summary(ϕ, js=colnames(ϕ); statistics=NamedTuple())
    ϕ = Tables.columntable(ϕ)
    map(js) do j
        θ = Tables.getcolumn(ϕ, j)
        merge((feature=j,),
              opnamedtuple(θ -> mean(abs.(θ)), θ, :meanabs),
              opnamedtuple(θ -> √(sum(θ.^2)), θ, :rms),
              opnamedtuple(std, θ),
              (opnamedtuple(s, θ, k) for (k,s) ∈ statistics)...
             )
    end
end
function summary(predict::Function, a::Algorithm, X; statistics=NamedTuple(), data=X)
    ϕ = shapley(predict, a, X, data)
    summary(ϕ; statistics)
end


export shapley

end # module
