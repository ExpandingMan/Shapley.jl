# these packages are not necessarily used by other methods
using Lasso, Distributions

"""
    KernelSHAP{P<:AbstractResource,R<:AbstractRNG,NT<:NamedTuple}

A `Shapley.Algorithm` object for describing the computation of Shapley values via the Kernel SHAP algorithm.

The `res` argument in the constructors specfies a computational resource, currently either `CPU1()` or `CPUThreads()` is
available.

This algorithm uses the lasso regression from Lasso.jl to perform regularized linear regressions.  Note that because this
algorithm involves a lasso fit for each individual data point, it is incredibly slow.

## Constructors
- `KernelSHAP(res, ν, M, rng=Random.GLOBAL_RNG; ϕ₀_colname=:ϕ₀, kwargs...)`
- `KernelSHAP(res, ν, M, rng=Random.GLOBAL_RNG; ϕ₀_colname=:ϕ₀, kwargs...)`

## Arguments
- `res`: An `AbstractResource` computational resource.  Pass `CPUThreads()` to run multi-threaded.
- `ν`: The number of distinct coalitions to use. It cannot excede `floor(N/2)` where `N` is the number of features.
- `M`: The multiplicity of sample data points per coalition.
- `ϕ₀_colname`: The column name to use for the intercept Shapley value. If `nothing`, colum will not be included.
- `kwargs`: Keyword arguments used by the fit.  The fit is performed by Lasso.jl, see [here](https://juliastats.org/Lasso.jl/stable/lasso/#StatsBase.fit) for available arguments.
"""
struct KernelSHAP{P,R<:AbstractRNG,NT<:NamedTuple} <: Algorithm{P}
    resource::P
    ν::Int       # the maximum number of elements to use in coalitions
    M::Int       # number of repetitions of each z
    fit_args::NT # arguments passed to the lass fit
    rng::R
    ϕ₀_colname::Union{Nothing,Symbol}  # column name to use for intercept shapley value

    function KernelSHAP(res::AbstractResource, ν::Integer, M::Integer, rng::AbstractRNG=Random.GLOBAL_RNG;
                        ϕ₀_colname::Union{Nothing,Symbol}=nothing, kwargs...)
        kwargs = (;kwargs...)  # wait for official 1.6 release to use NamedTuple here
        new{typeof(res),typeof(rng),typeof(kwargs)}(res, ν, M, kwargs, rng, ϕ₀_colname)
    end
end
KernelSHAP(ν::Integer, M::Integer, rng::AbstractRNG=Random.GLOBAL_RNG; kwargs...) = KernelSHAP(CPU1(), ν, M, rng; kwargs...)

"""
    coalitions!(r, z, n, k, α)

Write all possible binary vectors to the rows of boolean matrix `z` containing `n` non-zeros starting from position
`k` and row `α`.  This can be used recursively to generate all possible permutations of binary vectors such that
those with a smaller number of non-zeros occur first without duplicates, see `coalitions`.
"""
function coalitions!(r::AbstractVector{Bool}, z::AbstractMatrix{Bool}, n::Integer, k::Integer, α::Integer)
    n < 1 && return α+1
    for i ∈ k:(size(z,2) - n + 1)
        z[α, :] .= r
        z[α, i] = 1
        α = coalitions!(z[α, :], z, n-1, i+1, α)
    end
    α
end

"""
    coalitions(N, M, ν=(N-1))

Returns a boolean matrix `z` the rows of which give all possible permutations of binary vectors of length `N` containing
up to `ν` non-zeros, where rows with a smaller number of non-zero elements come before rows with a larger number of
non-zero elements and there are no duplicates.  The resulting matrix is repeated `M` times.

The rows of `z` are the "coalition" vectors needed by the KernelSHAP method such that they can be used to provide the best
possible approximation for a fixed `ν`.
"""
function coalitions(N::Integer, M::Integer, ν::Integer=(N-1))
    if ν > fld(N, 2)  # ensure that user doesn't inadvertently create duplicates
        throw(ArgumentError("degree $ν must be ≤ $(fld(N,2))"))
    end
    z = zeros(Bool, sum(binomial.(N, 1:ν)), N)
    α = 1
    for n ∈ 1:ν
        α = coalitions!(zeros(Bool, N), z, n, 1, α)
    end
    repeat([z; .!z], M)
end
coalitions(m::KernelSHAP, N::Integer) = coalitions(N, m.M, m.ν)

"""
    h!(ξt, rng, zr, Xt=ξt)

Perform the stochastic replacement of data in accordance with the row of the coalition matrix `zr` in accordance with the
Kernel SHAP method.
"""
function h!(ξt::Columns, rng::AbstractRNG, zr::AbstractVector{Bool}, Xt::Columns=ξt)
    n = Tables.rowcount(ξt)
    p = rand(rng, 1:Tables.rowcount(Xt), n)
    for (j, c) ∈ enumerate(ξt), i ∈ 1:n
        ξt[j][i] = Xt[j][zr[j] ? i : p[i]]
    end
    ξt
end

"""
    fh(f, rng, z, X, Z)

Call the prediction function `f` on data points with stochastic replacement from the function `h!`.
"""
function fh(f::Function, rng::AbstractRNG, z::AbstractMatrix{Bool}, X, Z)
    ξ = similartable(Z)
    ξt, Xt = compatibletable.((ξ, X))
    [f(h!(ξt, rng, zr, Xt)) for zr ∈ eachrow(z)]
end

"""
    defaultkernel(M, zc)

The weighting kernel used by the kernel SHAP method.  The resulting weight function grants more importance to coalitions
in which fewer features participate.
"""
defaultkernel(M::Integer, zc::Integer) = (M - 1) / (binomial(M, zc) * zc * (M - zc))

function _rhs_arg(rhs, i::Integer)
    r1 = first(rhs)
    v = Vector{eltype(r1)}(undef, length(rhs))
    for j ∈ eachindex(v)
        v[j] = rhs[j][i]
    end
    v
end

"""
    fitpoint(m::KernelSHAP, lhs, rhs)
    fitpoint(m::KernelSHAP, lhs, rhs, i)

Perform the fit for the Kernel SHAP methods on the data provided by `fitargs`.  If `i` is provided, this will be done for
the specific data point `i`.
"""
fitpoint(m::KernelSHAP, lhs, rhs) = fit(LassoModel, lhs, rhs; m.fit_args...)
fitpoint(m::KernelSHAP, lhs, rhs, i::Integer) = fitpoint(m, lhs, _rhs_arg(rhs, i))

"""
    fitargs(f, m::KernelSHAP, X, Z, z)

Generate the arguments needed by the kernel SHAP method to perform a fit.
"""
function fitargs(f::Function, m::KernelSHAP, X, Z, z)
    κ = .√(defaultkernel.(ncolumns(Z), sum(z, dims=2)))
    lhs = κ .* z
    rhs = κ .* fh(f, m.rng, z, X, Z)
    lhs, rhs
end

"""
    fitresultrow(m::KernelSHAP, sch::Tables.Schema, lhs, rhs, i)

Return the end result of kernel SHAP for a particular data point.
"""
function fitresultrow(m::KernelSHAP, sch::Tables.Schema, lhs, rhs, i::Integer)
    c = coef(fitpoint(m, lhs, rhs, i))
    n = length(sch.names)
    δ = length(c) > n ? 1 : 0  # first coefficient is intercept, but it may not be included
    nt = (;(sch.names[j]=>c[j + δ] for j ∈ 1:n)...)
    isnothing(m.ϕ₀_colname) ? nt : merge(nt, (;m.ϕ₀_colname=>first(c)))
end

function shapley(predict::Function, m::KernelSHAP, X, Z=X)
    z = coalitions(m, ncolumns(Z))
    lhs, rhs = fitargs(predict, m, X, Z, z)
    sch = Tables.schema(Z)
    _map(m.resource)(i -> fitresultrow(m, sch, lhs, rhs, i), 1:nrows(Z))
end

# NOTE: we require these methods per algorithm to resolve method and table ambiguities
function shapley(predict::Function, a::KernelSHAP, X, j::FeatureIndex, Z=X)
    st = shapley(predict, a, X, Z)
    [s[j] for s ∈ st]  # assumes the table returned above is an iterator of rows
end
